package main

import (
    // "crypto/tls"
    "fmt"
    "gopkg.in/mgo.v2"
    "gopkg.in/mgo.v2/bson"
    // "net"
)

func main() {

    // var DbClusters  []DatabaseCluster

// type DatabaseCluster struct {
//     Id              bson.ObjectId   `json:"_id" bson:"_id"`
//     Mongo           string          `json:"mongo" bson:"mongo"`
//     Redis           string          `json:"redis" bson:"redis"`
//     MQRouterKey     string          `json:"MQRouterKey" bson:"MQRouterKey"`
//     MQExchange      string          `json:"MQExchange" bson:"MQExchange"`
//     MQQueueName     string          `json:"MQQueueName" bson:"MQQueueName"`
//     MaxApps         int             `json:"maxApps" bson:"maxApps"`
//     NumberApps      int             `json:"numberApps" bson:"numberApps"`
//     Active          bool            `json:"isActive" bson:"isActive"`
// }

    Host := []string{
        "94.126.157.202:27030",
        "94.126.157.202:27031",
    }
    const (
        Username = "admin"
        Password = "12345678"
        Database = "main"
        Collection = "servers"
    )
    fmt.Printf("Connecting")
    session, err := mgo.DialWithInfo(&mgo.DialInfo{
        Addrs:    Host,
        Username: Username,
        Password: Password,
        Database: Database,
    })
    if err != nil {
        panic(err)
    }
    defer session.Close()

    session.SetMode(mgo.Strong, true)
    session.SetBatch(200)
    session.SetPrefetch(0.25)
    
    coll := session.DB(Database).C(Collection)
    fmt.Printf("FIND")
    // Find the number of games won by Dave
    // player := "Dave"
    gamesWon, err := coll.Find(bson.M{}).Count()
    if err != nil {
        fmt.Printf("Can't find")
        panic(err)
    }

    fmt.Printf("has won %d games.\n", gamesWon)
}
